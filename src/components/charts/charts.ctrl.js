export default {
  data () {
    return {
      mywidth: 300,
      myheight: 500,
      line: {
        mylabel: 'Line Chart Test',
        mylabels: ['happy', 'myhappy', 'hello'],
        mydata: [100, 40, 60]
      },
      bar: {
        mylabel: 'Bar Chart Test',
        mylabels: ['happy', 'myhappy', 'hello'],
        mydata: [100, 40, 60]
      },
      radar: {
        first_data: [10, 90, 60, 60],
        first_backgroundcolor: 'rgba(255,99,132,0.2)',
        first_bordercolor: 'rgba(255,99,132,1)'
      },
      labels: ['January', 'February', 'March'],
      datasets: [
        {
          backgroundColor: ['blue', 'red', 'yellow'],
          data: [40, 20, 100],
          width: 20,
          height: 20

        }
      ],
      myoption: {
        title: {
          display: true,
          position: 'bottom',
          text: 'Months'
        }
      }
    }
  }
}
